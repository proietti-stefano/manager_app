import { Injectable } from '@angular/core';
import { ICompany } from '../interfaces/i-company';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  companies: ICompany[] = [];

  constructor() { }

  getAll() {
    let localCompanies = localStorage.getItem('LocalCompany');
    if (localCompanies !== null) {
      this.companies = JSON.parse(localCompanies)
    }
    return this.companies
  }

  addCompany(newCompany: ICompany) {
  let newId = 0;
  this.companies.forEach(i => {
    if(i.id >= newId) {
      newId = i.id;
    }
  })
  newCompany.id = newId+1;
    return this.companies.push(newCompany);
  }

  deleteCompany(company: ICompany) {
    return this.companies.splice(this.companies.indexOf(company), 1)
  }

}
