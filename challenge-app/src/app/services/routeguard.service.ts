import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouteguardService implements CanActivate {

  private login = false;
  constructor() { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.login;
  }

  loginApp(username: string, password: string) {
    if (username === 'Admin' && password === 'user123') {
      this.login = true;
    } else {
      alert('Username e/o password errati')
    }
  }
}
