import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../services/companies.service';
import { Company } from '../classes/company';
import { ICompany } from '../interfaces/i-company';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  myCompany: Company[] = [];

  id!: number;
  name: string = '';
  address: string = '';
  phone!: number;
  revenue!: number;



  constructor(
    private companiesService: CompaniesService
  ) { }

  ngOnInit(): void {
    this.myCompany = this.companiesService.getAll()
    console.log(this.myCompany)

  }

  newCompany() {
    let myCompany: Company = new Company(0, this.name, this.address, this.phone, this.revenue);
    this.companiesService.addCompany(myCompany);
    let compJson = JSON.stringify(this.myCompany);
    localStorage.setItem('LocalCompany', compJson);
    //console.log(this.companiesService.getAll())
  }

  delCompany(item: ICompany) {
    this.companiesService.deleteCompany(item);
    let compJson = JSON.stringify(this.myCompany);
    localStorage.setItem('LocalCompany', compJson);
  }


}
