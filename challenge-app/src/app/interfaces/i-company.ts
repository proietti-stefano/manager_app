export interface ICompany {
    id: number;
    name: string;
    address: string;
    phone: number;
    revenue: number;
}
