import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouteguardService } from '../services/routeguard.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;

  username: string = '';
  password: string = '';

  constructor(
    private routeGuardService: RouteguardService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  loginUser() {
    //alert(this.username + ' ' + this.password)
    this.routeGuardService.loginApp(this.username, this.password)
    this.router.navigate(['home'])
  }



}
