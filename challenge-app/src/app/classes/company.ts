export class Company {

    id: number;
    name: string;
    address: string;
    phone: number;
    revenue: number;

    constructor(id: number, name: string, address: string, phone: number, revenue: number) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.revenue = revenue;
    }

}
